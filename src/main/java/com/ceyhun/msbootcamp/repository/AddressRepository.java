package com.ceyhun.msbootcamp.repository;

import com.ceyhun.msbootcamp.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
