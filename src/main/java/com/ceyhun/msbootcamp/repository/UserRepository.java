package com.ceyhun.msbootcamp.repository;

import com.ceyhun.msbootcamp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findUserById(Long id);
}
