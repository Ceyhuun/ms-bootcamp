package com.ceyhun.msbootcamp.repository;

import com.ceyhun.msbootcamp.model.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long> {
}
