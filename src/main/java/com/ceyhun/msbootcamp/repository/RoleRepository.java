package com.ceyhun.msbootcamp.repository;

import com.ceyhun.msbootcamp.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
