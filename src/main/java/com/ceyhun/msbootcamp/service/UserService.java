package com.ceyhun.msbootcamp.service;

import com.ceyhun.msbootcamp.model.User;

public interface UserService {

    void addUser(User user);

    User findUserById(Long id);
}
