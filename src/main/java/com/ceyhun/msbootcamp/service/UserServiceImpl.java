package com.ceyhun.msbootcamp.service;

import com.ceyhun.msbootcamp.model.User;
import com.ceyhun.msbootcamp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public User findUserById(Long id) {
        return userRepository.findUserById(id);
    }

    @Override
    public void addUser(User user) {
        userRepository.save(user);
    }
}
