package com.ceyhun.msbootcamp.model;

import liquibase.pro.packaged.A;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import net.bytebuddy.implementation.bind.annotation.FieldValue;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String firstName;
    String lastName;
    String username;
    String email;
    String password;
    Integer age;
    Double salary;

    @OneToOne
    @JoinColumn(name = "id")
    Address address;

    @OneToMany(mappedBy = "user")
    List<PhoneNumber> numbers;

    @ManyToMany
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id "))

    List<Role> roles;
}
